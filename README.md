The script is used to repackage binaries provided by Mozilla
as Slackware packages.

The direct link: http://download-origin.cdn.mozilla.net/pub/mozilla.org/firefox/releases/.

It can be used to upgrade system version of firefox with a newer one.
